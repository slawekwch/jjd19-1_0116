class Car {
    String name;
    int power;
    int speed;

    Car(String n, int p, int s) {
        name = n;
        power = p;
        speed = s;
    }

    void show() {
        System.out.println("Samochód " + name +  " " + power + " " + speed);
    }

    void turbo() {
//        int oldPower = power;
        power = power + (int)(power * 0.1);
//        return oldPower;
    }

    void increaseSpeed(int sp) {
        speed = speed + sp;
    }
}
