class CarTest {
    public static void main(String[] args) {
        Car car1 = new Car("Audi A4", 120, 100);
        car1.show();

        car1.turbo();

        car1.show();
        car1.increaseSpeed(25);
        car1.show();
    }
}
