class Person {
    String firstName;
    String lastName;
    int age;
    String city;

    Person(String firstName, String lastName, int age) {
        this.firstName = firstName;
        this.lastName = lastName;
        this.age = age;
    }

    Person(String firstName, String lastName, int age, String city) {
        this(firstName, lastName, age);
        this.city = city;
    }

    void increaseAge(int age) {
        this.age++;
    }


}
