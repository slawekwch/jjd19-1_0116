class Employee {
    String firstName;
    String lastName;
    double salary;

    Employee(String fn, String ln, double s) {
        firstName = fn;
        lastName = ln;
        salary = s;
    }
}
