class TimeExample {
    public static void main(String[] args) {
        Person person1 = new Person("John", "Kowalski", 20, "Wrocław");
        TimeMachine timeMachine = new TimeMachine();
        timeMachine.timeTravel(person1, 5);
        System.out.println(person1.age);
    }
}
